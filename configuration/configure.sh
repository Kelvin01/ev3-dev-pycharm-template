#!/usr/bin/env bash
echo "configure git email address and name" && git config --global user.email "robot@ev3dev.com"
git config --global user.name "robot"
echo "delete git-dir" && rm -rf /home/robot/project
echo "delete working-tree" && rm -rf /home/robot/.project.git
echo "create git dir" && mkdir /home/robot/.project.git
echo "initialize git dir" && git init --bare /home/robot/.project.git
echo "create project folder" && mkdir /home/robot/project
echo  "create git-hook" && cp /home/robot/configuration/.git-hook.template /home/robot/.project.git/hooks/post-receive
echo "make git hook executable"
chmod +x /home/robot/.project.git/hooks/post-receive
chmod +s /home/robot/.project.git/hooks/post-receive

echo "clone project to a temp folder"
git clone /home/robot/.project.git /home/robot/temp
cd /home/robot/temp
echo "create main.py file from template" && cp /home/robot/configuration/.project-template/main.py /home/robot/temp/main.py
git add .
git commit -m "creating template main.py"
git push origin master
echo "delete the temp folder"
rm -rf /home/robot/temp
cd /home/robot